from django.shortcuts import render
from django.http import HttpResponse


def narisi(request):
    """
    :param request: visina smreke
    :return:
    vrne stran, ki izpiše smreke
    """
    message = ''
    if request.method == "POST":
        t = request.POST['visina']
        try:
            n = int(t)
            if n > 100 or n < 1:
                return HttpResponse("Višina smrečice mora biti podana s številom od 1 do 100. <a "
                                    "href='/../'>Nazaj</a>")
        except ValueError:
            return HttpResponse("Višina smrečice mora biti podana s številom od 1 do 100. <a "
                                "href='/../'>Nazaj</a>")
        for i in range(n):
            message += 2*i*'&nbsp;'+(2*(n-i)-1)*'*'+'</br>'
        message += "<form action = '/../'><input type = 'submit' value = 'Nazaj' / ></form>"
    else:
        message = "Nekaj ne štima. <a href='/../'>Nazaj</a>"
    return HttpResponse(message)


def index(request):
    return render(request, 'index.html')

